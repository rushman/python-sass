#LDFLAGS="-Llibsass" python setup.py nosetests -s
LDFLAGS="-Llibsass"

.PHONY: libsass

all: libsass ext

libsass:
	$(MAKE) -C $@

ext:
	LDFLAGS=$(LDFLAGS) python setup.py build